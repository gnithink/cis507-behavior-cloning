** Behavioral Cloning**

As part of the project for the seminar I will be extending my project from my AI- CIS571
class about pacman game. As part of the course work, we design pacman agents to
find the optimal route between two points in a maze, implement the concepts of
reinforcement learning.
I would like to extend the project to implement behavioral cloning for pacman.
Behavioral cloning involves mimicking the behavior of various pacman agents by using
recorded games as training examples. I will design a classifier which will try to
determine the action taken by the observed agent. I will be designing the perceptron,
MIRA and a slightly modified version of the perceptron classifier. Then, the first two
classifiers are tested on a set of scanned handwritten digit images to recognize digits.
The modified version of the perceptron classifier will be tested on recorded pacman
games from various agents.

Link to the problem statement:
http://ai.berkeley.edu/classification.html


As part of the project for the CIS507 seminar, I wanted to implement behavioral cloning in pacman. I used a modified version of the perceptron classifier to train the pacman agent on a set of pre-recorded games that are available in the directory classification/pacmandata in the uploaded files. Then the pacman would clone the actions on which it is trained on and try to win the game. I implemented the perceptron classifier algorithm and the pacman is able to emerge victorious most of the time.

Link to the problem statement:
http://ai.berkeley.edu/classification.html


To implement the submitted code :
Download the repository and run the following command in the terminal:

python pacman.py -p ClassifierAgent --agentArgs agentToClone=ContestAgent

PS: Python version 2.7 is needed and I used the pycharm IDE to view and implement the code. Python-tk package is also needed. We can install python-tk package using the following command in the terminal:

pip install python-tk



---